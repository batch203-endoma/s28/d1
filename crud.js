//crud operations

/*

C-REATE
R-EAD
U-PDATE
D-ELETE


  -CRUD OPERATIONS ARE THE HEART OF ANY BACKEND APPLICATION.



*/



//[SECTION] Insert a documents (Create)

/*

  -Syntax:
    -db.collecntionName.insertOne({object});
    comparison with javascript
    Object.object.method({object});

*/

db.users.insertOne({
  firstName: "Jane",
  lastName: "Doe",
  age: 21,
  contact: {
    phone: "87654321",
    email: "janedoe@gmail.com"
  },
  courses: ["CSS", "Javascript", "Phython"],
  department: "none"

});


/*
Mini Activity

Scenario: We will create a database that will simulate a hotel database.
  1. Create a new database called "hotel".
  2. Insert a single room in the "rooms" collection with the following details:



  3. Use the "db.getCollection('users').find({})" query to check if the document is created.

  4. Take a screenshot of the Robo3t result and send it to the batch hangouts.







*/

db.rooms.insertOne({
  "name": "single",
  "accommodates": 2,
  "price": 1000,
  "description": "A simple room with basic necessities",
  "rooms_available": 10,
  "isAvailable": false
});



// Insert many

/*
  -snytax
    -db.collectionName.insertMany([{objectA, Objecttb}])
  */

db.users.insertMany([{
  firstName: "Stephen",
  lastName: "Hawking",
  age: 76,
  contact: {
    phone: "87654321",
    email: "stephenwaking@gmail.com"
  },
  courses: ["Phyton", "React", "PHP"],
  department: "none"

}, {
  firstName: "Neil",
  lastName: "Armstrong",
  age: 82,
  contact: {
    phone: "87654321",
    email: "neilarmstrong@gmail.com"
  },
  courses: ["React", "Laravel", "PHP"],
  department: "none"

}]);

/*
1. Using the hotel database, insert multiple room in the "rooms" collection with the following details:

                //Room 1:

                name - double
                accomodates - 3
                price - 2000
                description - A room fit for a small family going on a vacation
                rooms_available - 5
                isAvailable - false

                //Room 2:

                name - queen
                accomodates - 4
                price - 4000
                description - A room with a queen sized bed perfect for a simple getaway
                rooms_available - 15
                isAvailable - false

            2. Use the "db.getCollection('users').find({})" query to check if the document is created.

            3. Take a screenshot of the Robo3t result and send it to the batch hangouts.\


*/

//Room 1:
db.rooms.insertMany([{
  name: "double",
  accomodates: 3,
  price: 2000,
  description: "A room fit for a small family going on a vacation",
  rooms_available: 5,
  isAvailable: false222222222222222
}, {
  //Room 2:
  name: "queen",
  accomodates: 4,
  price: 4000,
  description: "A room with a queen sized bed perfect for a simple getaway",
  rooms_available: 15,
  isAvailable: false
}]);


//

// [SECTION] Retrieve a document(Read)

/*

  -Syntax
    -db.collectionName.find({}); // get all the documents
    -db.collectionName.find({field:value});// get a specific document.



*/

// find all documents in the collection
db.users.find({});

db.users.find({
  firstName: "Stephen"
});
db.users.find({
  department: "none"
});

//find documents with multiple parameters

/*

  -sytax:
    -db.collectionName.find({fieldA:valueA, fieldB:valueB})

*/

db.users.find({
  lastName: "Armstrong",
  age: 82
});


// [SECTION] updating documents (UPDATE)


//create document to UPDATE

db.users.insertOne({
  firstName: "Test",
  lastName: "Test",
  age: 0,
  contact: {
    phone: "0000000",
    email: "Test@gmail.com"
  },
  course: [],
  department: "none"
})


/*
  -Syntax:
    -db.collectionName.updateOne (){criteria}, {$set: {field:value}});
*/

db.users.updateOne({
  firstName: "Test"
}, {
  $set: {
    firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contacnt: {
      phone: "12345678",
      email: "bill@gmail.com"
    },
    course: ["PHP", "Laravel", "HTML"],
    department: "Operations",
    status: "active"
  }
})

// updating multiple documents
// - syntax = db.users.updateMany({criteria},{$set: {field:value}})
/*

*/

db.users.updateMany({
  department: "none"
}, {
  $set: {
    department: "HR"
  }
});




//replace One

// - syntax = db.users.replaceOne({criteria},{$set: {field:value}})



db.users.replaceOne({
  firstName: "Bill"
}, {
  firstName: "Bill",
  lastName: "Gates",
  age: 65,
  contacnt: {
    phone: "12345678",
    email: "bill@gmail.com"
  },
  courses: ["PHP", "Laravel", "HTML"],
  department: "Operations",
});




/*
    Mini Activity:

        1. Using the hotel database, update the queen room and set the available rooms to zero.

        2. Use the find query to validate if the room is successfully updated.

        3. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/

db.rooms.updateOne({
  name: "queen"
}, {
  $set: {
    rooms_available: 0
  }
});



//[SECTION] Removing documents (Delete) // delete the first document found.

db.users.deleteOne({
  firstName: "Test"
})

//

db.users.deleteMany({
  firstName: "Test"
});

/*

    Mini Activity:

    1. Using the hotel database, delete all rooms with 0 available rooms.

    2. Use the find query to show all the rooms and check if the 0 available room is deleted.

    3. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/

db.rooms.deleteMany({
  rooms_available: 0
});
//deletes many

db.rooms.deleteMany({
  rooms_available: 0
});

// [SECTION] advanced queries (subdocument)


// query an embedded document
db.users.find({
  contant: {
    phone: "87654321",
    email: "stephenhawking @gmail.com"

  }
})

//query on nested field

db.users.find({
  {
    contact.email: "stephenhawking"
  }
})

//querying an array with exact element

db.users.find({
  "courses": ["CSS", "JavaScript", "Phython"]
})


// querying an array disregarding the array elements order.

db.users.find(
  {
    courses: {$all: ["Javascript", "CSS", "Phython"]}
  }
)

//


//querying an embedded array

db.users.insertOne (
  {
    nameArr:[
      {
        nameA:"Juan"
      },
      {
        nameB:"Tamad"
      }
    ]
  }
)


db.users.find(
  {
    nameArr:{
      nameA:"Juan"
    }
  }
)
